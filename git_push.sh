#!/usr/bin/env bash

# cache login data
git config --global credential.helper "cache --timeout=7200"

git submodule update --remote --merge

#git pull

# git up
!git remote update -p; git merge --ff-only @{u}

git add -A .
git status
git commit -am "yspmedia.gitlab.io v.`date '+%d. %B %Y %H:%M %Z'`"
git push

exit ${?}
